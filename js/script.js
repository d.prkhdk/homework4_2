/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?
localStorage немає терміну зберігання даних і зберігає їх навть після закриттся або перезеавнтаження сторінки,
sessionStorage існує лише на поточній вкладці браузера.
Дані зберігаються після оновлення сторінки, але не закриття/відкриття вкладки.
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
Краще не зберігати чутливі дані через localStorage і sessionStorage, то му, що такі дані будуть вразливими для зловмисників.
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
null
тобто нічого

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/

const darkMode = document.querySelector("#dark-mode");
const body = document.body;
const paragraph = document.querySelectorAll("p");
const links = document.querySelectorAll("a");
const h1 = document.querySelector("h1");

const savedTheme = localStorage.getItem('theme');
if (savedTheme === 'dark') {
    body.classList.add('dark-theme');
    h1.classList.add('dark-theme');
    h1.style.background = "none";
    paragraph.forEach((p) => {
        p.classList.add("dark-theme");
        p.style.background = "none";
    });
    links.forEach((a) => {
        a.classList.add("dark-theme");
        a.style.background = "none";
    });
} else {
    body.classList.add('light-theme');
}

darkMode.addEventListener("click", () => {
    body.classList.toggle('dark-theme');
    body.classList.toggle('light-theme');

    paragraph.forEach((p) => {
        p.classList.toggle("dark-theme");
        p.style.background = "none";
    });
    links.forEach((a) => {
        a.classList.toggle("dark-theme");
        a.style.background = "none";
    });
    h1.classList.toggle('dark-theme');
    h1.classList.toggle('light-theme');

    if (body.classList.contains('dark-theme')) {
        localStorage.setItem('theme', 'dark');
    } else {
        localStorage.setItem('theme', 'light');
    }
});
